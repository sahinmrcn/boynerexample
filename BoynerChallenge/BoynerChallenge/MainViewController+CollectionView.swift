//
//  MainViewController+CollectionView.swift
//  BoynerChallenge
//
//  Created by sahin on 23.10.2021.
//

import UIKit


//MARK: - UICollectionView Data Source
extension MainViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return setupService ? newsType.count : 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NewsTypeCollectionViewCell.reuseIdentifier, for: indexPath) as! NewsTypeCollectionViewCell
        let text = setupService ? newsType[indexPath.row] : "General"
        cell.setup(isSetup: setupService, isSelect: isSelectIndex(type: text) != nil, title: text)
        
        return cell
    }
    
    func isSelectIndex(type: String) -> Int? {
        return selectType.firstIndex {
            return $0 == type
        }
    }
}

//MARK: - UICollectionView Delegate Flow Layout
extension MainViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let text = setupService ? newsType[indexPath.row] : "General"
        let width = widthForView(text: text, font: UIFont.systemFont(ofSize: 17))
        return CGSize(width: width + 50, height: 70)
    }
    
    func widthForView(text : String, font : UIFont) -> CGFloat {
        let height = font.lineHeight
        let label : UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: CGFloat.greatestFiniteMagnitude, height: height))
        label.font = font
        label.text = text
        label.sizeToFit()
        
        return label.frame.width
    }
}

//MARK: - UICollectionView Delegate Flow Layout
extension MainViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row < newsType.count {
            let text = newsType[indexPath.row]
            let index = isSelectIndex(type: text)
            
            if let index = index {
                selectType.remove(at: index)
            } else {
                selectType.append(newsType[indexPath.row])
            }
            fillSelectNews()
            
            collectionViewType.reloadData()
            tableViewNews.reloadData()
        }
    }
}
