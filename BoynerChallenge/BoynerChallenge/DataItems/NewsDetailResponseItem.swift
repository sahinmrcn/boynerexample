//
//  NewsDetailResponseItem.swift
//  BoynerChallenge
//
//  Created by sahin on 23.10.2021.
//

import Foundation

struct NewsDetailResponseItem: Codable {
    let status: String?
    let articles: [NewsDetailItem]?
}

struct NewsDetailItem: Codable {
    let author: String?
    let title: String?
    let description: String?
    let url: String?
    let urlToImage: String?
    let publishedAt: String?
    let content: String?
}
