//
//  NewsResponseItem.swift
//  BoynerChallenge
//
//  Created by sahin on 23.10.2021.
//

import Foundation

struct NewsResponseItem: Codable {
    let status: String?
    let sources: [NewsItem]?
}

struct NewsItem: Codable {
    let id: String?
    let name: String?
    let description: String?
    let url: String?
    let category: String?
    let language: String?
    let country: String?
}
