//
//  NewsDetailTableViewCell.swift
//  BoynerChallenge
//
//  Created by sahin on 23.10.2021.
//

import UIKit

class NewsDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var detailView: NewsDetailView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    static var reuseIdentifier: String {
        return String(describing: self)
    }
    
    func setup(detailItem: NewsDetailItem?) {
        detailView.setupDetail(isSetup: true, item: detailItem)
    }
}
