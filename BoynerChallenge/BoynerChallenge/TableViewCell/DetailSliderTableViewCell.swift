//
//  DetailSliderTableViewCell.swift
//  BoynerChallenge
//
//  Created by sahin on 23.10.2021.
//

import UIKit
import Kingfisher

protocol DetailSliderTableViewCellDelegate: AnyObject {
    func sliderSelect(sliderItems: NewsDetailItem)
}

class DetailSliderTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionViewSlider: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    weak var detailSliderTableViewCellDelegate: DetailSliderTableViewCellDelegate?
    var sliderItems: [NewsDetailItem] = []
    var isSetup = false
    var timer: Timer? = nil
    var nextCellIndex : Int = 0
    var isScroll : Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    static var reuseIdentifier: String {
        return String(describing: self)
    }
    
    func setup(sliderItems: [NewsDetailItem], isSetup: Bool) {
        self.sliderItems = sliderItems
        self.isSetup = isSetup
        setupXib()
        pageControl.numberOfPages = isSetup ? sliderItems.count : 1
        
        if !sliderItems.isEmpty {
            timerReset()
        }
    }
    
    func setupXib() {
        self.collectionViewSlider.register(UINib(nibName: SliderItemCollectionViewCell.reuseIdentifier, bundle: nil), forCellWithReuseIdentifier: SliderItemCollectionViewCell.reuseIdentifier)
        collectionViewSlider.reloadData()
    }
}

//MARK: - UICollectionView Data Source
extension DetailSliderTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return isSetup ? sliderItems.count : 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SliderItemCollectionViewCell.reuseIdentifier, for: indexPath) as! SliderItemCollectionViewCell
        cell.setup(isSetup: isSetup, item: isSetup ? sliderItems[indexPath.row] : nil)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    }
}

//MARK: - UICollectionView Delegate Flow Layout
extension DetailSliderTableViewCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: 250)
    }
}

//MARK: - UICollectionView Delegate
extension DetailSliderTableViewCell: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row < sliderItems.count {
            detailSliderTableViewCellDelegate?.sliderSelect(sliderItems: sliderItems[indexPath.row])
        }
    }
}

extension DetailSliderTableViewCell: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageWidth = collectionViewSlider.bounds.width
        let page = Int((collectionViewSlider.contentOffset.x + pageWidth / 2) / pageWidth)
        pageControl.currentPage = page
        nextCellIndex = page
        
        guard let timer = timer else {
            return
        }
        
        
        if timer.isValid && !isScroll {
            timer.invalidate()
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if !(timer?.isValid)! {
            startTimer()
        }
    }
}

extension DetailSliderTableViewCell {
    func timerReset() {
        timer?.invalidate()
        startTimer()
    }
    
    func startTimer() {
        timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(sliderScroll), userInfo: nil, repeats: true)
    }
    
    @objc func sliderScroll() {
        nextCellIndex = nextCellIndex + 1 == sliderItems.count ? 0 : nextCellIndex + 1
        let indexPath = IndexPath(row: nextCellIndex, section: 0)
        
        isScroll = true
        collectionViewSlider.scrollToItem(at: indexPath, at: .left, animated: true)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.isScroll = false
        }
    }
}
