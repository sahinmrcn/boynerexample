//
//  NewsTitleTableViewCell.swift
//  BoynerChallenge
//
//  Created by sahin on 23.10.2021.
//

import UIKit
import SkeletonView

class NewsTitleTableViewCell: UITableViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelContent: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    static var reuseIdentifier: String {
        return String(describing: self)
    }

    func setup(isSetup: Bool, title: String, content: String) {
        
        if isSetup {
            labelTitle.hideSkeleton()
            labelContent.hideSkeleton()
            
            labelTitle.text = title
            labelContent.text = content
        } else {
            labelTitle.showSkeleton()
            labelContent.showSkeleton()
        }
    }
}
