//
//  EmptyTableViewCell.swift
//  BoynerChallenge
//
//  Created by sahin on 23.10.2021.
//

import UIKit

class EmptyTableViewCell: UITableViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    static var reuseIdentifier: String {
        return String(describing: self)
    }
    
    func setup() {
        labelTitle.text = "Aradğınız kriterlerde sonuç bulunamadı."
    }
}
