//
//  NewsDetailView.swift
//  BoynerChallenge
//
//  Created by sahin on 23.10.2021.
//

import UIKit
import SkeletonView

class NewsDetailView: UIView {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imageTitle: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    
    //MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setup()
    }
    
    func setup() {
        Bundle.main.loadNibNamed("NewsDetailView", owner: self, options: nil)
        addSubview(containerView)
        containerView.frame = self.bounds
        containerView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    func setupDetail(isSetup: Bool, item: NewsDetailItem?) {
        if isSetup {
            imageTitle.hideSkeleton()
            
            labelTitle.text = item?.title ?? ""
            configDate(releaseDate: item?.publishedAt)
            
            if let imageURL = URL(string: item?.urlToImage ?? "") {
                imageTitle.kf.setImage(with: imageURL, placeholder: UIImage(), options: [.transition(.fade(1))])
            }
            
        } else {
            imageTitle.showSkeleton()
        }
    }

    func configDate(releaseDate: String?) {
        let timeZone: TimeZone = TimeZone(abbreviation: "GMT-3") ?? TimeZone.current
        guard let releaseDate = releaseDate,
        let convertDate = DateHelper.stringToDate(dateFormat: "yyyy-MM-dd'T'HH:mm:ss'Z'", dateString: releaseDate, timeZone: timeZone) else {
            labelDate.text = ""
            return
        }
        
        let convertString = DateHelper.dateToString(dateFormat: "hh:mm:ss", dateNow: convertDate)
        labelDate.text = convertString
    }
}
