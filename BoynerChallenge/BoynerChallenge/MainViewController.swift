//
//  MainViewController.swift
//  BoynerChallenge
//
//  Created by sahin on 23.10.2021.
//

import UIKit
import Alamofire

class MainViewController: UIViewController {

    @IBOutlet weak var collectionViewType: UICollectionView!
    @IBOutlet weak var tableViewNews: UITableView!
    
    lazy var apiService: NewsListService = {
            return NewsListService()
        }()
    
    var setupService = false
    var newsItems: [String : [NewsItem]] = [:]
    var newsType: [String] = []
    var selectNews: [NewsItem] = []
    var selectType: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }
    
    func setup() {
        setupXib()
        newsService()
        self.title = "Kaynaklar"
    }
    
    func setupXib() {
        collectionViewType.register(UINib(nibName: NewsTypeCollectionViewCell.reuseIdentifier, bundle: nil), forCellWithReuseIdentifier: NewsTypeCollectionViewCell.reuseIdentifier)
        collectionViewType.reloadData()
        
        tableViewNews.alwaysBounceVertical = false
        tableViewNews.register(UINib(nibName: NewsTitleTableViewCell.reuseIdentifier, bundle: nil), forCellReuseIdentifier: NewsTitleTableViewCell.reuseIdentifier)
        tableViewNews.register(UINib(nibName: EmptyTableViewCell.reuseIdentifier, bundle: nil), forCellReuseIdentifier: EmptyTableViewCell.reuseIdentifier)
        tableViewNews.reloadData()
    }
}

extension MainViewController {
    func newsToService(items: [NewsItem]?) {
        setupService = true
        guard let items = items else {
            collectionViewType.reloadData()
            tableViewNews.reloadData()
            return
        }

        let filterNews = items.filter({
            return $0.country == "us"
        })
        
        newsItems = Dictionary(grouping: filterNews, by: { $0.category ?? "" })
        
        newsType.removeAll()
        newsType = newsItems.map({
            return $0.key
        })
        
        if let type = newsType.first {
            selectType.append(type)
            fillSelectNews()
        }
        
        collectionViewType.reloadData()
        tableViewNews.reloadData()
    }
    
    func fillSelectNews() {
        selectNews.removeAll()
        for item in selectType {
            selectNews.append(contentsOf: newsItems[item] ?? [])
        }
    }
    
    func newsService() {
        apiService.newsService { [weak self] success, sources, error in
            if success {
                self?.newsToService(items: sources)
            }
        }
    }
}
