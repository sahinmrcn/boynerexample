//
//  NewsListService.swift
//  BoynerChallenge
//
//  Created by sahin on 24.10.2021.
//

import Foundation
import Alamofire

class NewsListService {
    func newsService( complete: @escaping ( _ success: Bool, _ sources: [NewsItem]?, _ error: Error? )->() ) {
        let serviceUrl = "https://newsapi.org/v2/sources?apiKey=5b50fb32bc1b4b2b8512cdfe2875427f"
        
        debugPrint(serviceUrl)
        
        AF.request(serviceUrl, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseData { [weak self] response in
            switch response.result {
            case .success(let data):
                let item = try! JSONDecoder().decode(NewsResponseItem.self, from: data)
                
                complete(true, item.sources, nil)
                
            case .failure(let error):
                ErrorHelper.showAlert(for: error, retryFunction: { self?.newsService(complete: complete) })
                complete(false, nil, error)
            }
        }
    }
}
