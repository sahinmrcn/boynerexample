//
//  NavigationHelper.swift
//  BoynerChallenge
//
//  Created by sahin on 23.10.2021.
//

import UIKit

class NavigationHelper {
    
    fileprivate static var _sharedInstance: NavigationHelper?
    static var sharedInstance: NavigationHelper {
        if _sharedInstance == nil {
            _sharedInstance = NavigationHelper()
        }
        return _sharedInstance!
    }
    
    static func reset() {
        _sharedInstance = nil
    }
    
    //MARK: - Variables
    var mainNavigationController: UINavigationController?
    
    //MARK: - Visible View Controller Finder Methods
    static func getVisibleContainerViewController(for rootViewController: UIViewController? = nil) -> UIViewController? {
        var rootVC = rootViewController
        if rootVC == nil {
            rootVC = UIApplication.shared.keyWindow?.rootViewController
        }
        
        if let presentedVC = rootVC?.presentedViewController {
            return getVisibleContainerViewController(for: presentedVC)
        } else {
            return rootVC
        }
    }
}

