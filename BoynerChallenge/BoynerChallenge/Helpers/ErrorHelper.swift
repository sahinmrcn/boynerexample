//
//  ErrorHelper.swift
//  BoynerChallenge
//
//  Created by sahin on 23.10.2021.
//

import UIKit


class ErrorHelper {
    static func showAlert(for error: Error, message: String = "", retryFunction: (() -> Void)? = nil, actions: [UIAlertAction] = []) {
        let errorCode = (error as NSError).code
        showAlert(for: errorCode, message: message, retryFunction: retryFunction, actions: actions)
    }
    
    static func showAlert(for errorCode: Int = 0, message: String = "", retryFunction: (() -> Void)? = nil, actions: [UIAlertAction] = []) {
        guard let uiAlert = getUIAlert(for: errorCode, message: message, retryFunction: retryFunction, actions: actions) else {
            return
        }
        let visibleViewController = NavigationHelper.getVisibleContainerViewController()
        visibleViewController?.present(uiAlert, animated: true, completion: nil)
    }
    
    fileprivate static func getUIAlert(for errorCode: Int, message: String, retryFunction: (() -> Void)?, actions: [UIAlertAction]) -> UIAlertController? {
        
        let uiAlert = UIAlertController(title: "Hata", message: message, preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: "Tamam", style: .cancel)
        uiAlert.addAction(dismissAction)
        
        if let retryFunction = retryFunction {
            let retryAction = UIAlertAction(title: "Tekrar Dene", style: .default, handler: { (action) in
                retryFunction()
            })
            uiAlert.addAction(retryAction)
        }
        
        for action in actions {
            uiAlert.addAction(action)
        }
        
        return uiAlert
    }
}

