//
//  DateHelper.swift
//  BoynerChallenge
//
//  Created by sahin on 23.10.2021.
//

import Foundation

class DateHelper {
    static func stringToDate(dateFormat: String, dateString: String, timeZone: TimeZone = TimeZone.current) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat =  dateFormat//"yyyy-MM-dd'T'HH:mm:ss.SS"
        dateFormatter.timeZone = timeZone
        guard let date = dateFormatter.date(from: dateString) else { return nil }
        
        return date
    }
    
    static func dateToString(dateFormat: String, dateNow: Date, timeZone: TimeZone = TimeZone.current) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat//"yyyy-MM-dd"
        dateFormatter.timeZone = timeZone
        dateFormatter.locale = Locale(identifier: "tr_TR")
        let dateString = dateFormatter.string(from: dateNow)
        
        return dateString
    }
}
