//
//  UIStackView+Extension.swift
//  BoynerChallenge
//
//  Created by sahin on 23.10.2021.
//

import UIKit

extension UIStackView {
    func removeAllArrangedSubviews() {
        for view in self.arrangedSubviews {
            self.removeArrangedSubview(view)
            view.isHidden = true
        }
        self.setNeedsLayout()
        self.layoutIfNeeded()
    }
}
