//
//  DetailViewController.swift
//  BoynerChallenge
//
//  Created by sahin on 23.10.2021.
//

import UIKit
import Alamofire

class DetailViewController: UIViewController {

    @IBOutlet weak var tableViewNews: UITableView!
    
    var item: NewsItem?
    var isSetup = false
    var sliderItems: [NewsDetailItem] = []
    var detailItems: [NewsDetailItem] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }
    
    func setup() {
        setupXib()
        newsService()
        self.title = item?.name ?? ""
    }
    
    func setupXib() {
        tableViewNews.alwaysBounceVertical = false
        tableViewNews.register(UINib(nibName: DetailSliderTableViewCell.reuseIdentifier, bundle: nil), forCellReuseIdentifier: DetailSliderTableViewCell.reuseIdentifier)
        tableViewNews.register(UINib(nibName: NewsDetailTableViewCell.reuseIdentifier, bundle: nil), forCellReuseIdentifier: NewsDetailTableViewCell.reuseIdentifier)
        tableViewNews.reloadData()
    }
}


//MARK: - UITableView Data Source
extension DetailViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return detailItems.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: DetailSliderTableViewCell.reuseIdentifier, for: indexPath) as! DetailSliderTableViewCell
            cell.setup(sliderItems: sliderItems, isSetup: isSetup)
            
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: NewsDetailTableViewCell.reuseIdentifier, for: indexPath) as! NewsDetailTableViewCell
        cell.setup(detailItem: detailItems[indexPath.row - 1])
        
        return cell
    }
}

//MARK: - UITableView Delegate
extension DetailViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}


extension DetailViewController {
    func newsToService(items: [NewsDetailItem]?) {
        isSetup = true
        
        guard let items = items else {
            tableViewNews.reloadData()
            return
        }
        sliderItems = items.count <= 3 ? items : Array(items.prefix(upTo: 3))
        detailItems = items.count <= 3 ? [] : Array(items.suffix(from: 3))

        tableViewNews.reloadData()
    }
    
    func newsService() {
        let category = item?.category ?? ""
        let serviceUrl = "https://newsapi.org/v2/top-headlines?country=us&category=\(category)&apiKey=5b50fb32bc1b4b2b8512cdfe2875427f"
        
        debugPrint(serviceUrl)
        
        AF.request(serviceUrl, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseData {  [weak self] response in
            switch response.result {
            case .success(let data):
                let item = try! JSONDecoder().decode(NewsDetailResponseItem.self, from: data)
                
                self?.newsToService(items: item.articles)
                
            case .failure(let error):
                ErrorHelper.showAlert(for: error, retryFunction: { self?.newsService() })
            }
        }
    }

}
