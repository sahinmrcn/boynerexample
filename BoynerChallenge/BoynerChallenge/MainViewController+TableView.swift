//
//  MainViewController+TableView.swift
//  BoynerChallenge
//
//  Created by sahin on 23.10.2021.
//

import UIKit

//MARK: - UITableView Data Source
extension MainViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let isEmtyCount = setupService ? 1 : 0
        return selectNews.isEmpty ? isEmtyCount : selectNews.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if selectNews.isEmpty {
            let cell = tableView.dequeueReusableCell(withIdentifier: EmptyTableViewCell.reuseIdentifier, for: indexPath) as! EmptyTableViewCell
            cell.setup()
            
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: NewsTitleTableViewCell.reuseIdentifier, for: indexPath) as! NewsTitleTableViewCell
        cell.setup(isSetup: setupService, title: selectNews[indexPath.row].name ?? "", content: selectNews[indexPath.row].description ?? "")
        
        return cell
    }
}

//MARK: - UITableView Delegate
extension MainViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row < selectNews.count {
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let detailVC = mainStoryBoard.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
            detailVC.item = selectNews[indexPath.row]
            navigationController?.pushViewController(detailVC, animated: true)
        }
    }
}
