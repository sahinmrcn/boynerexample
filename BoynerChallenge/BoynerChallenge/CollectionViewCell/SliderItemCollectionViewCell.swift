//
//  SliderItemCollectionViewCell.swift
//  BoynerChallenge
//
//  Created by sahin on 23.10.2021.
//

import UIKit
import SkeletonView

class SliderItemCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var detailView: NewsDetailView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    static var reuseIdentifier: String {
        return String(describing: self)
    }
    
    func setup(isSetup: Bool, item: NewsDetailItem?) {
        detailView.setupDetail(isSetup: isSetup, item: item)
    }
}
