//
//  NewsTypeCollectionViewCell.swift
//  BoynerChallenge
//
//  Created by sahin on 23.10.2021.
//

import UIKit
import SkeletonView

class NewsTypeCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageTitle: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var viewBack: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    static var reuseIdentifier: String {
        return String(describing: self)
    }
    
    func setup(isSetup: Bool, isSelect: Bool, title: String) {
        
        if isSetup {
            labelTitle.hideSkeleton()
            imageTitle.hideSkeleton()
            
            let plus = #imageLiteral(resourceName: "plus")
            let check = #imageLiteral(resourceName: "check")
            
            imageTitle.image = isSelect ? check : plus
            viewBack.backgroundColor = isSelect ? .black : .white
            labelTitle.textColor = isSelect ? .white : .black
            labelTitle.text = title
        } else {
            labelTitle.showSkeleton()
            imageTitle.showSkeleton()
        }
    }
}
